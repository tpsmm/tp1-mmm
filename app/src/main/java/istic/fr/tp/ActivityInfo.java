package istic.fr.tp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import Module.User;

public class ActivityInfo extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information);


        Bundle data = getIntent().getExtras();
        User user = (User) data.getParcelable("user");

        TextView nom = (TextView) findViewById(R.id.nametxt);
        TextView prenom = (TextView) findViewById(R.id.lastnametxt);
        TextView dateNaissance = (TextView) findViewById(R.id.birthdatetxt);
        TextView ville = (TextView) findViewById(R.id.citytxt);

        nom.setText(user.getFirstName().toString());
        prenom.setText(user.getLastName().toString());
        dateNaissance.setText(user.getBirthDate().toString());
        ville.setText(user.getCity().toString());
    }
}
