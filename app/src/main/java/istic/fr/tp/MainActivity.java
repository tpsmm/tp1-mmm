package istic.fr.tp;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import Module.User;

public class MainActivity extends AppCompatActivity {

    int d;
    int m;
    int y;
    EditText nom;
    EditText prenom;
    RelativeLayout ll;
    EditText ed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ll = (RelativeLayout) findViewById(R.id.rl);

        nom = (EditText) findViewById(R.id.nomedittxt);
        prenom = (EditText) findViewById(R.id.prenomedittxt);
        final CalendarView datenaissance = (CalendarView) findViewById(R.id.simpleCalendarView);
        ed = new EditText(getApplicationContext());


        datenaissance.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {

            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month,
                                            int dayOfMonth) {

                d = dayOfMonth;
                m = month;
                y = year;
            }
        });


        final Spinner spinner = (Spinner) findViewById(R.id.spinner);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {

                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://fr.wikipedia.org/wiki/" + spinner.getSelectedItem().toString()));
                // startActivity(intent);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        Button valider = (Button) findViewById(R.id.validebtn);

        valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /**
                 * Exercice 2
                 */
//                Toast.makeText(getApplicationContext(), "Nom complet : " + nom.getText() + " " + prenom.getText()
//                                + ", date de naissance : " + d + "/" + (m + 1) + "/" + y +
//                                ", N° :  " + ed.getText(),
//                        Toast.LENGTH_LONG).show();

                /**
                 * Exercice 3
                 */
                Intent myIntent = new Intent(MainActivity.this, ActivityInfo.class);
                myIntent.putExtra("user", new User(nom.getText().toString(), prenom.getText().toString(), d + "/" + (m + 1) + "/" + y, spinner.getSelectedItem().toString()));
                MainActivity.this.startActivity(myIntent);

            }
        });
    }

    void addEditText(RelativeLayout rel, EditText ed) {
        ActionBar.LayoutParams lParamsMW = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        ed.setLayoutParams(lParamsMW);
        ed.setTextColor(Color.BLACK);
        ed.setText("Numéro");
        rel.addView(ed);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.reset) {

            nom.setText("");
            prenom.setText("");

            return true;
        }

        if (id == R.id.addEditText) {

            addEditText(ll, ed);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
