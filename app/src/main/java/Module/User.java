package Module;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by bouluad on 30/01/17.
 */
public class User implements Parcelable {
    
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        public User[] newArray(int size) {
            return new User[size];
        }
    };
    private String firstName;
    private String lastName;
    private String birthDate;
    private String city;

    public User(String firstName, String lastName, String birthDate, String city) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.city = city;
    }

    protected User(Parcel in) {
        String[] data = new String[4];

        in.readStringArray(data);
        this.firstName = data[0];
        this.lastName = data[1];
        this.birthDate = data[2];
        this.city = data[3];
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringArray(new String[]{this.firstName,
                this.lastName,
                this.birthDate,
                this.city});
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
